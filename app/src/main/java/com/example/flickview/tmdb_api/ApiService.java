package com.example.flickview.tmdb_api;

import com.example.flickview.movie_list.Movie;
import com.example.flickview.movie_list.MoviesResponse;
import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.movie_list.movie_details.credit.CreditResponse;
import com.example.flickview.movie_list.movie_details.review.ReviewResponse;
import com.example.flickview.movie_list.movie_details.trailer.TrailerResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("movie/popular")
    Observable<MoviesResponse> getPopularMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    @GET("movie/top_rated")
    Observable<MoviesResponse> getTopRatedMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    @GET("movie/upcoming")
    Observable<MoviesResponse> getUpcomingMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    @GET("genre/movie/list")
    Observable<GenresResponse> getGenres(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
    @GET("search/movie")
    Observable<MoviesResponse> getMoviesBasedOnQuery(
            @Query("api_key") String api_key,
            @Query("query") String q
    );
    @GET("movie/{movie_id}")
    Observable<Movie> getMovie(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("movie/{movie_id}/videos")
    Observable<TrailerResponse> getTrailers(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("movie/{movie_id}/reviews")
    Observable<ReviewResponse> getReviews(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("movie/{movie_id}/credits")
    Observable<CreditResponse> getCredits(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey
    );
}
