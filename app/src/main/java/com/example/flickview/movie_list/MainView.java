package com.example.flickview.movie_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.flickview.R;

import com.example.flickview.movie_list.callbacks.OnMoviesClickCallback;
import com.example.flickview.movie_list.genre.Genre;
import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.movie_list.movie_details.MovieActivity;
import com.example.flickview.movie_list.movie_details.MovieView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

class MainView implements IMainView {
    private Context context;
    private Activity activity;
    MainPresenter presenter;
    SearchView searchViewQ;
    private MoviesAdapter moviesAdapter;
    private static String POPULAR = "popular";
    private static String TOP_RATED = "top_rated";
    private static String UPCOMING = "upcoming";
    @BindView(R.id.movies_list)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;

    private String sortBy = POPULAR;
    private List<Genre> movieGenres;
    private boolean isFetchingMovies = true;
    private int currentPage = 1;

    MainView(View view, Activity activity) {
        this.context = view.getContext();
        this.activity = activity;
        presenter = new MainPresenter(this);
        unbinder = ButterKnife.bind(this, view);
        setupOnScrollListener();
        getGenres();
        presenter.getMovieList(sortBy, currentPage);
    }

    private void getGenres() {
        presenter.getGenreList();
    }

    @SuppressLint("CheckResult")
    private void setupOnScrollListener() {
        final LinearLayoutManager manager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(moviesAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = manager.getItemCount();
                int visibleItemCount = manager.getChildCount();
                int firstVisibleItem = manager.findFirstVisibleItemPosition();
                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    if (!isFetchingMovies) {
                        //presenter.getMovies(currentPage + 1,moviesRepository,sortBy);
                        currentPage++;
                        if(currentPage > 1){
                            presenter.getMovieList(sortBy, currentPage);
                        }
                    }
                }
            }
        });
    }

    void showSortMenu() {
        PopupMenu sortMenu = new PopupMenu(context, activity.findViewById(R.id.sort));
        sortMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                currentPage = 1;
                switch (item.getItemId()) {
                    case R.id.popular:
                        sortBy = POPULAR;
                        //presenter.getMovies(currentPage,moviesRepository,sortBy);
                        presenter.getMovieList(sortBy, currentPage);
                        return true;
                    case R.id.top_rated:
                        sortBy = TOP_RATED;
                        //presenter.getMovies(currentPage,moviesRepository,sortBy);
                        presenter.getMovieList(sortBy, currentPage);
                        return true;
                    case R.id.upcoming:
                        sortBy = UPCOMING;
                        //presenter.getMovies(currentPage,moviesRepository,sortBy);
                        presenter.getMovieList(sortBy, currentPage);
                        return true;
                    default:
                        sortBy = POPULAR;
                        presenter.getMovieList(sortBy, currentPage);
                        return false;
                }
            }
        });
        sortMenu.inflate(R.menu.menu_movies_sort);
        sortMenu.show();
        currentPage = 1;
    }

    @Override
    public void setTitle() {
        switch (sortBy) {
            case "popular":
                activity.setTitle("Popular");
                break;
            case "top_rated":
                activity.setTitle("Top Rated");
                break;
            case "upcoming":
                activity.setTitle("Upcoming");
                break;
        }
    }

    @Override
    public void displayQueryMovies(MoviesResponse moviesResponse) {
        moviesAdapter.clearMovies();
        moviesAdapter = new MoviesAdapter(moviesResponse.getMovies(), movieGenres, callback);
        recyclerView.setAdapter(moviesAdapter);
        moviesAdapter.notifyDataSetChanged();
    }

    @Override
    public void displayGenres(GenresResponse genresResponse) {
        if (genresResponse != null) {
            movieGenres = genresResponse.getGenres();
        }
    }

    @Override
    public void displayMovies(int page, MoviesResponse moviesResponse) {
        if(moviesResponse!=null){
            if (moviesAdapter == null) {
                moviesAdapter = new MoviesAdapter(moviesResponse.getMovies(), movieGenres, callback);
                recyclerView.setAdapter(moviesAdapter);
            } else {
                if (page == 1) {
                    moviesAdapter.clearMovies();
                }
                moviesAdapter.appendMovies(moviesResponse.getMovies());
            }
            currentPage = moviesResponse.getPage();
            isFetchingMovies = false;
            setTitle();
        }
    }

    private OnMoviesClickCallback callback = new OnMoviesClickCallback() {
        @Override
        public void onClick(Movie movie) {
            Intent intent = new Intent(context, MovieActivity.class);
            intent.putExtra(MovieView.MOVIE_ID, movie.getId());
            activity.startActivity(intent);
        }
    };
}
