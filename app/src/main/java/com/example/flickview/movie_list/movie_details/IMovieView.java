package com.example.flickview.movie_list.movie_details;

import com.example.flickview.movie_list.Movie;
import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.movie_list.movie_details.credit.CreditResponse;
import com.example.flickview.movie_list.movie_details.review.ReviewResponse;
import com.example.flickview.movie_list.movie_details.trailer.TrailerResponse;


public interface IMovieView {
    void loadError(String message);
    void showTrailer(String url);

    void displayCredits(CreditResponse creditResponse);
    void displayGenres(GenresResponse genresResponse,Movie movie);
    void displayReviews(ReviewResponse reviewResponse);
    void displayTrailers(TrailerResponse trailerResponse);
    void displayMovie(Movie movie);

}
