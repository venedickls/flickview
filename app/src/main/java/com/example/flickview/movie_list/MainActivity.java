package com.example.flickview.movie_list;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.example.flickview.R;

public class MainActivity extends AppCompatActivity {

    MainView mainView;
    View view;
    @SuppressLint("InflateParams")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = getLayoutInflater().inflate(R.layout.activity_main,null);
        setContentView(view);
        mainView = new MainView(view,this);
        setSupportActionBar(mainView.toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movies, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mainView.searchViewQ = (SearchView) menu.findItem(R.id.search_movie)
                .getActionView();
        mainView.searchViewQ.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        mainView.searchViewQ.setMaxWidth(Integer.MAX_VALUE);
        mainView.searchViewQ.setQueryHint("Enter movie name..");
        mainView.presenter.getResults(mainView.searchViewQ);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort:
                mainView.showSortMenu();
                return true;
            case R.id.search_movie:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainView.unbinder.unbind();
    }
}
