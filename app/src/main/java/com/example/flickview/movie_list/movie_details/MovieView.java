package com.example.flickview.movie_list.movie_details;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.flickview.R;
import com.example.flickview.movie_list.Movie;
import com.example.flickview.movie_list.genre.Genre;
import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.movie_list.movie_details.credit.Credit;
import com.example.flickview.movie_list.movie_details.credit.CreditResponse;
import com.example.flickview.movie_list.movie_details.review.Review;
import com.example.flickview.movie_list.movie_details.review.ReviewResponse;
import com.example.flickview.movie_list.movie_details.trailer.Trailer;
import com.example.flickview.movie_list.movie_details.trailer.TrailerResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MovieView implements IMovieView {
    private Context context;
    private MoviePresenter presenter;
    private Activity activity;
    public static String MOVIE_ID = "movie_id";
    private static String YOUTUBE_VIDEO_URL = "http://www.youtube.com/watch?v=%s";
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.movieDetailsBackdrop) ImageView movieBackdrop;
    @BindView(R.id.movieDetailsTitle) TextView movieTitle;
    @BindView(R.id.movieDetailsGenre) TextView movieGenre;
    @BindView(R.id.movieDetailsOverview) TextView movieOverview;
    @BindView(R.id.summaryLabel) TextView movieOverviewLabel;
    @BindView(R.id.movieDetailsReleaseDate) TextView movieReleaseDate;
    @BindView(R.id.movieDetailsRating) RatingBar movieRating;
    @BindView(R.id.movieTrailers) LinearLayout movieTrailers;
    @BindView(R.id.movieReviews) LinearLayout movieReviews;
    @BindView(R.id.movieCreditsx) LinearLayout movieCredits;
    @BindView(R.id.trailersLabel) TextView trailersLabel;
    @BindView(R.id.reviewsLabel) TextView reviewsLabel;
    @BindView(R.id.creditsLabel) TextView creditsLabel;
    Unbinder unbinder;
    private int movieId;

    MovieView(View view, final Activity activity) {
        this.activity = activity;
        this.context = view.getContext();
        presenter = new MoviePresenter(this);
        unbinder = ButterKnife.bind(this,view);
        movieId = activity.getIntent().getIntExtra(MOVIE_ID,movieId);
        presenter.getMovie(movieId);

    }

    @Override
    public void loadError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTrailer(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
    }

    @Override
    public void displayCredits(CreditResponse creditResponse) {
        creditsLabel.setVisibility(View.VISIBLE);
        movieCredits.removeAllViews();
        for(Credit credit: creditResponse.getCast()){
            View parent = activity.getLayoutInflater().inflate(R.layout.credit,movieCredits,false);
            ImageView profile_path = parent.findViewById(R.id.creditsProfile);
            TextView cast_name = parent.findViewById(R.id.creditsName);
            if(credit.getProfile_path() != null) {
                cast_name.setText(String.format("%s as %s", credit.getName(), credit.getCharacter()));
                String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w500";
                Glide.with(activity).load(IMAGE_BASE_URL + credit.getProfile_path())
                        .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                        .into(profile_path);
                movieCredits.addView(parent);
            }
        }
    }


    @Override
    public void displayGenres(GenresResponse genresResponse,Movie movie) {
        if(movie.getGenres()!=null){
            List<String> currentGenres = new ArrayList<>();
            for(Genre genre: movie.getGenres()){
                currentGenres.add(genre.getName());
            }
            movieGenre.setText(TextUtils.join(",",currentGenres));
        }
    }

    @Override
    public void displayReviews(ReviewResponse reviewResponse) {
        reviewsLabel.setVisibility(View.VISIBLE);
        movieReviews.removeAllViews();
        for (Review review : reviewResponse.getReviews()) {
            View parent = activity.getLayoutInflater().inflate(R.layout.review, movieReviews, false);
            TextView author = parent.findViewById(R.id.reviewAuthor);
            TextView content = parent.findViewById(R.id.reviewContent);
            author.setText(review.getAuthor());
            content.setText(review.getContent());
            movieReviews.addView(parent);
        }
    }

    @Override
    public void displayTrailers(TrailerResponse trailerResponse) {
        trailersLabel.setVisibility(View.VISIBLE);
        movieTrailers.removeAllViews();
        for (final Trailer trailer: trailerResponse.getTrailers()){
            View parent = activity.getLayoutInflater().inflate(R.layout.thumbnail_trailer,movieTrailers,false);
            ImageView thumbnail = parent.findViewById(R.id.thumbnail);
            thumbnail.requestLayout();
            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTrailer(String.format(YOUTUBE_VIDEO_URL,trailer.getKey()));
                }
            });
            String YOUTUBE_THUMBNAIL_URL = "http://img.youtube.com/vi/%s/0.jpg";
            Glide.with(activity)
                    .load(String.format(YOUTUBE_THUMBNAIL_URL,trailer.getKey()))
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary).centerCrop())
                    .into(thumbnail);
            movieTrailers.addView(parent);
        }
    }

    @Override
    public void displayMovie(Movie movie) {
        movieTitle.setText(movie.getTitle());
        movieOverview.setText(movie.getOverview());
        movieOverviewLabel.setVisibility(View.VISIBLE);
        movieRating.setVisibility(View.VISIBLE);
        movieRating.setRating(movie.getRating()/2);
        presenter.getRxGenres(movie);
        movieReleaseDate.setText(movie.getReleaseDate());
        if(!activity.isFinishing()){
            String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w780";
            Glide.with(activity)
                    .load(IMAGE_BASE_URL + movie.getBackdrop())
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(movieBackdrop);
        }
        presenter.getRxTrailers(movie);
        presenter.getRxReviews(movie);
        presenter.getRxCredits(movie);
    }
}
