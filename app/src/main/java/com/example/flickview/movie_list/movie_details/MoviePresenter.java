package com.example.flickview.movie_list.movie_details;

import com.example.flickview.movie_list.Movie;
import com.example.flickview.movie_list.NetworkClient;
import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.movie_list.movie_details.credit.CreditResponse;
import com.example.flickview.movie_list.movie_details.review.ReviewResponse;
import com.example.flickview.movie_list.movie_details.trailer.TrailerResponse;
import com.example.flickview.tmdb_api.ApiService;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MoviePresenter implements IMoviePresenter {
    private IMovieView view;

    private static final String TMDB_API_KEY = "8f7bde2cf9f6d3ca40c3bb03c55abb51";
    private static String LANGUAGE = "en-US";

    MoviePresenter(IMovieView view) {
        this.view = view;
    }


    @Override
    public void getRxTrailers(Movie movie) {
        trailerResponseObservable(movie).subscribe(trailerResponseObserver());
    }

    @Override
    public void getRxReviews(Movie movie) {
        reviewResponseObservable(movie).subscribe(reviewResponseObserver());
    }

    @Override
    public void getRxGenres(Movie movie) {
        genresResponseObservable().subscribe(genresResponseObserver(movie));
    }

    @Override
    public void getRxCredits(Movie movie) {
        creditResponseObservable(movie).subscribe(creditResponseObserver());
    }

    @Override
    public void getMovie(int movieId) {
        movieObservable(movieId).subscribe(movieObserver());
    }

    private Observable<Movie> movieObservable(int movieId){
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getMovie(movieId,TMDB_API_KEY,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observer<Movie> movieObserver(){
        return new Observer<Movie>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Movie movie) {
                view.displayMovie(movie);
            }

            @Override
            public void onError(Throwable e) {
                view.loadError("Error:"+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };

    }
    private Observable<CreditResponse> creditResponseObservable(Movie movie){
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getCredits(movie.getId(),TMDB_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observer<CreditResponse> creditResponseObserver(){
        return new Observer<CreditResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CreditResponse creditResponse) {
                view.displayCredits(creditResponse);
            }

            @Override
            public void onError(Throwable e) {
                view.loadError("Error:"+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private Observable<TrailerResponse> trailerResponseObservable(Movie movie){
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getTrailers(movie.getId(),TMDB_API_KEY,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observer<TrailerResponse> trailerResponseObserver(){
        return new Observer<TrailerResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(TrailerResponse trailerResponse) {
                view.displayTrailers(trailerResponse);
            }

            @Override
            public void onError(Throwable e) {
                view.loadError("Error:"+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private Observable<ReviewResponse> reviewResponseObservable(Movie movie){
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getReviews(movie.getId(),TMDB_API_KEY,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()
                );
    }

    private Observer<ReviewResponse> reviewResponseObserver(){
        return new Observer<ReviewResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ReviewResponse reviewResponse) {
                view.displayReviews(reviewResponse);
            }

            @Override
            public void onError(Throwable e) {
                view.loadError("Error:"+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private Observable<GenresResponse> genresResponseObservable(){
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getGenres(TMDB_API_KEY,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Observer<GenresResponse> genresResponseObserver(final Movie movie){
        return new Observer<GenresResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(GenresResponse genresResponse) {
                view.displayGenres(genresResponse,movie);
            }

            @Override
            public void onError(Throwable e) {
                view.loadError("Error:"+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
