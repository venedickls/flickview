package com.example.flickview.movie_list.callbacks;

import com.example.flickview.movie_list.Movie;

public interface OnMoviesClickCallback {
    void onClick(Movie movie);
}
