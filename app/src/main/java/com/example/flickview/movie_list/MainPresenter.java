package com.example.flickview.movie_list;


import android.util.Log;
import android.widget.SearchView;

import com.example.flickview.movie_list.genre.GenresResponse;
import com.example.flickview.tmdb_api.ApiService;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;


public class MainPresenter implements IMainPresenter {
    private IMainView mainView;

    private static final String TMDB_API_KEY = "8f7bde2cf9f6d3ca40c3bb03c55abb51";
    private static String LANGUAGE = "en-US";
    int mPage = 1;
    MainPresenter(IMainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void getMovieList(String sortBy, int page) {
        switch (sortBy) {
            case "top_rated":
                topRatedMoviesObservable(page).subscribe(moviesResponseDisposableObserver(page));
                break;
            case "upcoming":
                upcomingMoviesObservable(page).subscribe(moviesResponseDisposableObserver(page));
                break;
            case "popular":
                popularMoviesObservable(page).subscribe(moviesResponseDisposableObserver(page));
                break;
            default:
                popularMoviesObservable(page).subscribe(moviesResponseDisposableObserver(page));
                break;
        }
    }

    private Observable<MoviesResponse> popularMoviesObservable(int page) {
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getPopularMovies(TMDB_API_KEY, LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Observable<MoviesResponse> topRatedMoviesObservable(int page) {
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getTopRatedMovies(TMDB_API_KEY, LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private Observable<MoviesResponse> upcomingMoviesObservable(int page) {
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getUpcomingMovies(TMDB_API_KEY, LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private DisposableObserver<MoviesResponse> moviesResponseDisposableObserver(final int page) {
        return new DisposableObserver<MoviesResponse>() {

            @Override
            public void onNext(MoviesResponse moviesResponse) {
                mainView.displayMovies(page, moviesResponse);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void getGenreList() {
        genresResponseObservable().subscribe(getObserver());
    }

    private Observable<GenresResponse> genresResponseObservable() {
        return NetworkClient.getRetrofit().create(ApiService.class)
                .getGenres(TMDB_API_KEY, LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                ;
    }

    private DisposableObserver<GenresResponse> getObserver() {
        return new DisposableObserver<GenresResponse>() {
            @Override
            public void onNext(GenresResponse genresResponse) {
                mainView.displayGenres(genresResponse);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

    }

    @Override
    public void getResults(SearchView searchView) {
        getObservableQuery(searchView)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) {
                        if(s.equals("")){
                            return false;
                        }else {
                            return true;
                        }
                    }
                })
                .debounce(2, TimeUnit.SECONDS)
                .distinctUntilChanged()
                .switchMap(new Function<String, ObservableSource<MoviesResponse>>() {
                    @Override
                    public ObservableSource<MoviesResponse> apply(@NonNull String s) {
                        return NetworkClient.getRetrofit().create(ApiService.class)
                                .getMoviesBasedOnQuery(TMDB_API_KEY,s);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserverResult());

    }

    private Observable<String> getObservableQuery(SearchView searchView){
        final PublishSubject<String> publishSubject = PublishSubject.create();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                publishSubject.onNext(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                publishSubject.onNext(newText);
                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                popularMoviesObservable(mPage).subscribe(moviesResponseDisposableObserver(mPage));
                return false;
            }
        });

        return publishSubject;
    }

    public Observer<MoviesResponse> getObserverResult(){
        return new Observer<MoviesResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(MoviesResponse moviesResponse) {
                mainView.displayQueryMovies(moviesResponse);
            }

            @Override
            public void onError(Throwable e) {
                Log.d("TAG","Error"+e);
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d("TAG","Completed");
            }
        };
    }

}
