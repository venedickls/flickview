package com.example.flickview.movie_list.movie_details.credit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Credit {
    @SerializedName("character")
    @Expose
    private String character;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("profile_path")
    @Expose
    private String profile_path;

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path(){
        return profile_path;
    }

    public void setProfile_path(String profile_path){
        this.profile_path = profile_path;
    }
}
