package com.example.flickview.movie_list;

import android.widget.SearchView;

public interface IMainPresenter {
    void getGenreList();
    void getMovieList(String sortBy,int page);
    void getResults(SearchView searchView);
}
