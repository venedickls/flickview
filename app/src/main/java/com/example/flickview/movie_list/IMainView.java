package com.example.flickview.movie_list;

import com.example.flickview.movie_list.genre.GenresResponse;


public interface IMainView {
    void setTitle();
    void displayQueryMovies(MoviesResponse moviesResponse);
    void displayGenres(GenresResponse genresResponse);
    void displayMovies(int page,MoviesResponse moviesResponse);
}
