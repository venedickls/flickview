package com.example.flickview.movie_list.movie_details;

import com.example.flickview.movie_list.Movie;
public interface IMoviePresenter {
    void getRxTrailers(Movie movie);
    void getRxReviews(Movie movie);
    void getRxGenres(Movie movie);
    void getRxCredits(Movie movie);

    void getMovie(int movieId);
}
