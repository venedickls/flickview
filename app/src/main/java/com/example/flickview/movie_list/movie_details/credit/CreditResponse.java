package com.example.flickview.movie_list.movie_details.credit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreditResponse {
    @SerializedName("cast")
    @Expose
    private List<Credit> credits;

    public List<Credit> getCast(){
        return credits;
    }

    public void setCredits(List<Credit> credits) {
        this.credits = credits;
    }
}
