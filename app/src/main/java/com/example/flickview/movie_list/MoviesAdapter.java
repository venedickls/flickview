package com.example.flickview.movie_list;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.flickview.movie_list.genre.Genre;
import com.example.flickview.movie_list.callbacks.OnMoviesClickCallback;
import com.example.flickview.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private List<Movie> movieList;
    private List<Genre> genreList;
    private OnMoviesClickCallback callback;
    MoviesAdapter(List<Movie> movieList,List<Genre> genreList,OnMoviesClickCallback callback) {
        this.callback = callback;
        this.movieList = movieList;
        this.genreList = genreList;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie,parent,false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.Bind(movieList.get(position));
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    void appendMovies(List<Movie> moviesToAppend){
        movieList.addAll(moviesToAppend);
        notifyDataSetChanged();
    }

    void clearMovies() {
        movieList.clear();
        notifyDataSetChanged();
    }


    class MovieViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_movie_year) TextView releaseDate;
        @BindView(R.id.item_movie_title) TextView title;
        @BindView(R.id.item_movie_rating) TextView rating;
        @BindView(R.id.item_movie_genre) TextView genres;
        @BindView(R.id.item_movie_thumbnail) ImageView poster;
        Movie movie;
        MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onClick(movie);
                }
            });
        }

        void Bind(Movie movie){
            this.movie = movie;
            releaseDate.setText(movie.getReleaseDate().split("-")[0]);
            title.setText(movie.getTitle());
            rating.setText(String.valueOf(movie.getRating()));
            genres.setText(getGenres(movie.getGenreIds()));
            String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w500";
            Glide.with(itemView).load(IMAGE_BASE_URL + movie.getPosterPath())
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(poster);
        }

        private String getGenres(List<Integer> genreIds){
            List<String> movieGenres = new ArrayList<>();
            for (Integer genreId:genreIds){
                for (Genre genre:genreList){
                    if(genre.getId() == genreId){
                        movieGenres.add(genre.getName());
                        break;
                    }
                }
            }
            return TextUtils.join(",",movieGenres);
        }


    }
}
